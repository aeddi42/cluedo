% Lancer la resolution puis si c'est bon, l'affichage
solution:-
	resolution(etat([peon, loup, chevre, choux], []), Etapes),
	write('Point de depart.'), nl,
	affichage(etat([peon, loup, chevre, choux], []), Etapes).


% Objectif a atteindre
resolution(etat([], [peon, loup, chevre, choux]), []).

% Resolution
resolution(Etat, [Elem|Etapes]):-
	resolution(Elem, Etapes),			% Verifie si l'objectif est atteint
	traverser(Etat, Elem).				% Sinon tente une taverse


% Tente une traverse seul vers la rive gauche
traverser(etat(Gauche, [peon|Droite]), etat([peon|Gauche], Droite)):-
	 non_dangereux(Droite).

% Tente une traverse avec un passager vers la rive droite
traverser(etat([peon|Gauche], Droite), etat(GaucheTmp, [peon|DroiteTmp])):-
	 select(Animal, Gauche, GaucheTmp),
	 select(Animal, DroiteTmp, Droite),
	 non_dangereux(GaucheTmp).

% Tente une traverse avec un passager vers la rive gauche
traverser(etat(Gauche, [peon|Droite]), etat([peon|GaucheTmp], DroiteTmp)):-
	 select(Animal, Droite, DroiteTmp),
	 select(Animal, GaucheTmp, Gauche),
	 non_dangereux(DroiteTmp).


% Verifie si l'etat de la rive a quitter est safe ou si un animal va mourir
non_dangereux(Etat):-
	\+(reunis(chevre, choux, Etat)),
	\+(reunis(loup, chevre, Etat)).

% Verifie si deux animaux incompatibles sont reunis
reunis(AnimalX, AnimalY, Etat):-
	member(AnimalX, Etat),
	member(AnimalY, Etat).


% Affiche l'etat, l'etape courant puis depile pour la prochaine etape
affichage(Etat, [Elem|Etapes]):-
	write(Etat), nl, nl,
	afficher_etape(Etat, Elem), nl,
	affichage(Elem, Etapes).

% Condition d'arret, si il ne reste plus qu'un element dans la pile
affichage(_, [Final]):-
	write(Final), nl.


% Affiche la traverse du peon seul
afficher_etape(etat(Gauche, [peon|Droite]), etat([peon|Gauche], Droite)):-
	write('Le brave peon retourne tout seul sur la rive gauche.').

% Affiche la traverse du peon et d'un passager a droite
afficher_etape(etat([peon|Gauche], _), etat(GaucheTmp, [peon|_])):-
	select(Animal, Gauche, GaucheTmp),
	write('Le brave peon va sur la rive droite avec le '),
	write(Animal).

% Affiche la traverse du peon et d'un passager a gauche
afficher_etape(etat(_, [peon|Droite]), etat([peon|_], DroiteTmp)):-
	select(Animal, Droite, DroiteTmp),
	write('Le brave peon retourne sur la rive gauche avec le '),
	write(Animal).