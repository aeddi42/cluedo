% Placement des indices dans la bonne maison
indice(Maison, [Maison|_]).
indice(Maison, [_|Queue]) :- indice(Maison, Queue).

% Défini que la maison A est à gauche de la maison B
a_gauche(A, B, [A, B|_]).
a_gauche(A, B, [_|Queue]) :- a_gauche(A, B, Queue).

% Défini que la maison A est voisine à la maison B
a_cote(A, B, Maisons) :- a_gauche(A, B, Maisons); a_gauche(B, A, Maisons).

% Défini la position absolue des maisons
position_absolue(A, B, C, D, E, [A, B, C, D, E]).

definir(Maisons):-
	Maisons = [_, _, _, _, _],
	indice(maison(rouge, anglais, _, _, _), Maisons),
	indice(maison(_, suedois, _, _, chiens), Maisons),
	indice(maison(_, danois, the, _, _), Maisons),
	a_gauche(maison(verte, _, _, _, _), maison(blanche, _, _, _, _), Maisons),
	indice(maison(verte, _, cafe, _, _), Maisons),
	indice(maison(_, _, _, pallmall, oiseaux), Maisons),
	indice(maison(jaune, _, _, dunhill, _), Maisons),
	position_absolue(_, _, maison(_, _, lait, _, _), _, _, Maisons),
	position_absolue(maison(_, norvegien, _, _, _), _, _, _, _, Maisons),
	a_cote(maison(_, _, _, blend, _), maison(_, _, _, _, chats), Maisons),
	a_cote(maison(_, _, _, _, cheval), maison(_, _, _, dunhill, _), Maisons),
	indice(maison(_, _, biere, bluemaster, _), Maisons),
	indice(maison(_, allemand, _, prince, _), Maisons),
	a_cote(maison(_, norvegien, _, _, _), maison(bleue, _, _, _, _), Maisons),
	a_cote(maison(_, _, _, blend, _), maison(_, _, eau, _, _), Maisons).

poisson_appartient_a(Proprietaire):-
 	definir(Maisons), 
 	indice(maison(_, Proprietaire, _, _, poisson), Maisons).
