% Defini les femmes
femme(anne).
femme(betty).
femme(lisa).
femme(sylvie).
femme(eve).
femme(julie).
femme(valerie).

% Defini tout ceux qui ne sont pas des femmes en hommes
homme(X):-
	\+femme(X).

% Defini qui sont les maris
mari_de(marc, anne).
mari_de(luc, betty).
mari_de(jules, lisa).
mari_de(leon, sylvie).
mari_de(loic, eve).
mari_de(paul, julie).

% Defini que toute femme d'un mari est sa femme (Yes c'est WTF)
femme_de(F, M):-
	mari_de(M, F).

% Defini qui est l'enfant d'un homme
enfant_de(jean, marc).
enfant_de(jules, marc).
enfant_de(leon, marc).
enfant_de(lisa, luc).
enfant_de(loic, luc).
enfant_de(gerard, luc).
enfant_de(jacques, jules).
enfant_de(herve, leon).
enfant_de(julie, leon).
enfant_de(paul, loic).
enfant_de(valerie, loic).

% Defini toute femme d'un homme qui a un enfant comme mere de ce dernier
enfant_de(E, M):-
	femme_de(M, P), enfant_de(E, P).

% Defini tout pere comme beau pere du conjoint de son enfant
beaupere_de(P, G):-
	enfant_de(E, P), mari_de(E, G), homme(P);
	enfant_de(E, P), mari_de(G, E), homme(P).

% Defini toute femme d'un beau pere comme une belle mere
bellemere_de(M, G):-
	femme_de(M, P), beaupere_de(P, G).

% Defini comme ancetre tout parent d'un enfant
ancetre_de(P, E):-
	enfant_de(E, P);
	enfant_de(E, X), ancetre_de(P, X).
